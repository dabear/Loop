//
//  BranchConfigTableViewController.swift
//  LoopUI
//
//  Created by Bjørn Inge Berg on 07/07/2019.
//  Copyright © 2019 LoopKit Authors. All rights reserved.
//
//
import UIKit
import LoopKitUI
import LoopKit
import LoopCore
import HealthKit

public protocol NibLoadable: IdentifiableClass {
    static func nib() -> UINib
}


extension NibLoadable {
    public static func nib() -> UINib {
        return UINib(nibName: className, bundle: Bundle(for: self))
    }
}


extension SwitchTableViewCell: NibLoadable {}

public class BranchConfigTableViewController: UITableViewController{
    
    
    
    
    
    public init() {
        super.init(style: .grouped)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        /*tableView.register(AlarmTimeInputRangeCell.nib(), forCellReuseIdentifier: AlarmTimeInputRangeCell.className)
        
        
        tableView.register(GlucoseAlarmInputCell.nib(), forCellReuseIdentifier: GlucoseAlarmInputCell.className)
        
        tableView.register(TextFieldTableViewCell.nib(), forCellReuseIdentifier: TextFieldTableViewCell.className)
        tableView.register(TextButtonTableViewCell.self, forCellReuseIdentifier: TextButtonTableViewCell.className)
        tableView.register(SegmentViewCell.nib(), forCellReuseIdentifier: SegmentViewCell.className)
        
        tableView.register(mmSwitchTableViewCell.nib(), forCellReuseIdentifier: mmSwitchTableViewCell.className)
        
        tableView.register(mmTextFieldViewCell.nib(), forCellReuseIdentifier: mmTextFieldViewCell.className)*/
        tableView.register(SwitchTableViewCell.nib(), forCellReuseIdentifier: SwitchTableViewCell.className)
        self.tableView.rowHeight = 44;
        
    }
    
    
    
    private enum BranchConfigRow : Int, CaseCountable {
        case applyX15Workaround
        
    }
    
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return BranchConfigRow.count
        
    }
    
  
    @objc private func shouldApplyX15WorkaroundChanged(_ sender: UISwitch) {
        print("shouldApplyX15Workaround changed to \(sender.isOn)")
        UserDefaults.standard.shouldApplyX15Workaround = sender.isOn
    }
    
    
   
    
    
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        switch BranchConfigRow(rawValue: indexPath.row)! {
        case .applyX15Workaround:
        
            let switchCell = tableView.dequeueReusableCell(withIdentifier: SwitchTableViewCell.className, for: indexPath) as! SwitchTableViewCell
            
            switchCell.switch?.isOn = UserDefaults.standard.shouldApplyX15Workaround
            //switchCell.titleLabel?.text = "test"
            
            switchCell.titleLabel?.text = NSLocalizedString("Apply x15-workaround", comment: "The title text for the application of the x15 workaround needed for some x15 pumps")
            
            switchCell.switch?.addTarget(self, action: #selector(shouldApplyX15WorkaroundChanged(_:)), for: .valueChanged)
            switchCell.contentView.layoutMargins.left = tableView.separatorInset.left
            return switchCell
        }
        
    }
    
    public override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return NSLocalizedString("Branch specific settings", comment: "The title text for the Branch specific settings")
        
        
    }
    
    public override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return nil
        
    }
    
    public override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch BranchConfigRow(rawValue: indexPath.row)! {
        
        case .applyX15Workaround:
            print("selected applyX15Workaround")
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}



